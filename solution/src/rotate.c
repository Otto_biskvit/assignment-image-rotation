#include "rotate.h"
#include <malloc.h>

struct image rotate(struct image const* im) {
    struct image res = init_image(im->height, im->width);

    for (uint32_t j = 0; j < im->height; j++) {
        for (uint32_t i = 0; i < im->width; i++) {
            *get_px(im->height-1-j,i,&res) = *get_px(i, j,im);
        }
    }
    return res;
}

