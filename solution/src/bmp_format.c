#include <bmp_format.h>
#include <inttypes.h>
#define BMP_FORMAT 0x4D42
#define PADDING 4
#define BMP_BITS 24
#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t count_padding(uint32_t width) {
    return (PADDING-(width*3)%PADDING)%PADDING;
}


struct bmp_header create_header(uint16_t width, uint16_t height) {
    uint16_t size = (sizeof(struct pixel)*width+count_padding(width))*height;
    return (struct bmp_header) {
            .bfType = BMP_FORMAT,
            .bfileSize = sizeof(struct bmp_header)+size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = BMP_BITS,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };}

enum bmp_read_status from_bmp(FILE* in, struct image* im) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_FAIL;

    if (header.bfType != BMP_FORMAT) return WRONG_TYPE;

    if (header.biBitCount != BMP_BITS) return INVALID_HEADER;



    *im = init_image(header.biWidth, header.biHeight);
    uint8_t padding = count_padding(header.biWidth);
    for (uint64_t i = 0; i < im->height; i++) {
        if (fread(get_px(0, i, im), sizeof(struct pixel), im->width, in) != im->width) return READ_FAIL;
        if (fseek(in, padding, SEEK_CUR)) return READ_FAIL;
    }

    return READ_OK;
}


enum bmp_write_status to_bmp(FILE* out, struct image const* im) {
    struct bmp_header header = create_header(im->width, im->height);
    if (fwrite(&header,sizeof(struct bmp_header),1,out) != 1){

        return WRITE_FAIL;}

    uint8_t padding = count_padding(im->width);
    uint8_t zerobyte[3] = {0};
    for (uint64_t i = 0; i < im->height; i++) {
        if (fwrite(get_px(0,i,im),sizeof(struct pixel),im->width,out) != im->width) return WRITE_FAIL;

        if (padding > 0) fwrite(&zerobyte, sizeof(uint8_t), padding, out);

    }
    return WRITE_OK;
}
