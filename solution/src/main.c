#include "file-work.h"
#include "bmp_format.h"
#include "rotate.h"
#include <stdbool.h>
#include <stdio.h>

void usage() {
    printf("Usage: ./image-transformer <source-image> <transformed-image>");
}

int check_file(enum file_status status){
    switch (status) {
        case OPEN_FAIL:
            fprintf(stderr, "Unsuccessful file open \n");
            return 1;
        case PERMISSION_DENIED:
            fprintf(stderr, "Permission denied \n");
            return 1;
        case NOT_EXIST:
            fprintf(stderr, "File not exist \n");
            return 1;
        case OPEN_OK:
            fprintf(stderr, "Successful file open \n");
            return 0;
        case CLOSE_FAIL:
            fprintf(stderr, "Unsuccessful file close \n");
            return 1;
        case CLOSE_OK:
            fprintf(stderr, "Successful file close \n");
            return 0;
        default: return 1;
    }
}


int check_bmp_read(enum bmp_read_status status){
    switch (status) {
        case READ_FAIL:
            fprintf(stderr, "Unsuccessful bmp read \n");
            return 1;
        case WRONG_TYPE:
            fprintf(stderr, "Wrong type of image \n");
            return 1;
        case INVALID_HEADER:
            fprintf(stderr, "Image has an invalid header \n");
            return 1;
        case READ_OK:
            fprintf(stderr, "Successful bmp read \n");
            return 0;
        default: return 1;
    }
}

int check_bmp_write(enum bmp_write_status status){
    switch (status) {
        case WRITE_FAIL:
            fprintf(stderr, "Unsuccessful bmp write \n");
            return 1;
        case WRITE_OK:
            fprintf(stderr, "Successful bmp write \n");
            return 0;
        default: return 1;

    }
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc != 3){
        usage();
        return 1;}

    FILE* file;
    FILE* rotate_file;



    if(check_file(file_open(&file, argv[1], "rb" ))) return 1;
    struct image im = {0};
    if(check_bmp_read(from_bmp(file, &im))){
        image_free(&im);
        file_close(&file);
        return 1;
    }
    struct image rotate_im = rotate(&im);
    image_free(&im);
    if (check_file(file_open(&rotate_file, argv[2], "wb"))){
        file_close(&file);
        image_free(&rotate_im);
        return 1;
    }
    if(check_bmp_write(to_bmp(rotate_file, &rotate_im))){
        image_free(&rotate_im);
        file_close(&file);
        file_close(&rotate_file);
        return 1;
    }
    image_free(&rotate_im);
    if (check_file(file_close(&file))) return 1;
    if (check_file(file_close(&rotate_file))) return 1;


    return 0;
}
