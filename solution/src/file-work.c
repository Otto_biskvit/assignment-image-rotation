#include "file-work.h"
#include <errno.h>

enum file_status file_open(FILE** in, const char* name, const char* mode) {
    *in = fopen(name, mode);
    errno = 0;
    if (errno == EACCES) return PERMISSION_DENIED;
    if (errno == ENOENT) return NOT_EXIST;
    if (*in != NULL) return OPEN_OK;

    return OPEN_FAIL;
}

enum file_status file_close(FILE** out) {
    if (*out) {
        if (fclose(*out)) {
            return CLOSE_FAIL;
        }
        return CLOSE_OK;
    }
    return NOT_EXIST;
}
