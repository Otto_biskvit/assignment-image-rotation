#include "image.h"
#include <malloc.h>



struct image init_image(uint32_t width, uint32_t height){
    struct image new_im = (struct image) {.width = width, .height = height, .data = malloc(width*height*sizeof(struct pixel))};
    return new_im;
}
struct pixel* get_px(uint32_t x, uint32_t y, struct image const* im) {
    return &im->data[y*im->width+x];
}
void image_free(struct image* im) {
    if (im->data != NULL) free(im->data);
}


