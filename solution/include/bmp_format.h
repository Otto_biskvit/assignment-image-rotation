#ifndef LABFIN2_BMP_FORMAT
#define LABFIN2_BMP_FORMAT

#include <stdint.h>
#include <stdio.h>
#include "image.h"

enum bmp_read_status  {
    READ_OK=0,
    READ_FAIL,
    WRONG_TYPE,
    INVALID_HEADER
};

enum bmp_write_status {
    WRITE_OK=0,
    WRITE_FAIL
};


enum bmp_read_status from_bmp(FILE* in, struct image* im);
enum bmp_write_status to_bmp(FILE* out, struct image const* im);

#endif //LABFIN2_BMP_FORMAT
