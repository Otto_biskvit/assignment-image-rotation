#ifndef LABFIN2_FILE_WORK
#define LABFIN2_FILE_WORK

#include <stdio.h>

enum file_status {
    OPEN_OK=0,
    OPEN_FAIL,
    CLOSE_OK,
    CLOSE_FAIL,
    PERMISSION_DENIED,
    NOT_EXIST
};

enum file_status file_open(FILE** in, const char* name, const char* mode);
enum file_status file_close(FILE** out);

#endif //LABFIN2_FILE_WORK
