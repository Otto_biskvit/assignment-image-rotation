
#ifndef LABFIN2_IMAGE
#define LABFIN2_IMAGE

#include <stdint.h>
#include <stdio.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel* get_px( uint32_t x, uint32_t y, struct image const* im );
struct image init_image( uint32_t width, uint32_t height );
void image_free(struct image* im);

#endif  //LABFIN2_IMAGE




