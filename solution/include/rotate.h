#ifndef  LABFIN2_ROTATE
#define LABFIN2_ROTATE

#include "image.h"

struct image rotate(struct image const* im);

#endif //LABFIN2_ROTATE
